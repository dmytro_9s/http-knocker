# http-knocker

## Overview

A standalone service used to fetch a series of HTTP endpoints at regular intervals.

The service uses an external configuration file to specify an array of URLs to grab.  The service spawns several threads (one for each target), each thread attempts to grab the destination URL up to a maximum number of retries, then it will sleep before starting again.

## Running

```sh
$ ./http-knocker --conf http-knocker.json
Successfully Opened knocker-config.json
{"level":"error","start":"2018-09-15T10:22:23.549424+12:00","duration":1.001767418,"retries":3,"max_retries":3,"status":"","target":"http://mail.ru"}
{"level":"info","start":"2018-09-15T10:22:21.548124+12:00","duration":3.043594231,"retries":1,"max_retries":5,"status":"200 OK","target":"http://amazon.com"}
{"level":"info","start":"2018-09-15T10:22:26.548134+12:00","duration":0.689058606,"retries":1,"max_retries":2,"status":"200 OK","target":"http://google.com"}
{"level":"info","start":"2018-09-15T10:22:26.548246+12:00","duration":0.825873168,"retries":1,"max_retries":5,"status":"200 OK","target":"http://amazon.com"}
```

## Building

The service was written in Go, hence a Go compiler is needed to build it:

```sh
$ go build
```

## Configuration

Below is a sample configuration file:

```json
[
	{
		"url": "http://google.com",
		"interval": 10,
		"retry": 2,
		"method": "GET"

	},
	{
		"url": "http://amazon.com",
		"interval": 5
	},
	{
		"url": "http://mail.ru",
		"interval": 5,
		"retry": 3,
		"timeout":1
	}
]

```

At a minimum, each entry must contain a `url` and `interval`.  If ommitted, if the `retry` will be set to `5`, the `timeout` to `10` seconds and the method to `GET`.

## Bugs

To be discovered

## Todo

- Add optional headers
- Configure destination to store response (eg: DB)

## Author

- Dmytro Kopylov
- Mourad Kessas