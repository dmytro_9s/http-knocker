package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const _conf = "knocker-config.json" // Default filename if none specified at command line

// Osp is a struct containing OSP endpoint definition
type Osp struct {
	Interval int32  `json:"interval"`
	Method   string `json:"method"`
	URL      string `json:"url"`
	Retry    int    `json:"retry"`
	Timeout  int    `json:"timeout"`
}

// LogEntry a log entry
type LogEntry struct {
	Level      string  `json:"level"`
	Start      string  `json:"start"`
	Duration   float64 `json:"duration"`
	Retries    int     `json:"retries"`
	MaxRetries int     `json:"max_retries"`
	Status     string  `json:"status"`
	Target     string  `json:"target"`
}

func main() {

	var config []Osp

	/* Read command line arguments or fallback to default value (--conf /path/to/config.json) */
	filename := flag.String("conf", _conf, "Configuration File")
	flag.Parse()

	/* Open file */
	data, err := os.Open(*filename)
	if err != nil {
		panic("Unable to open '" + *filename + "': " + err.Error())
	}
	defer data.Close()
	fmt.Printf("Successfully Opened %s\n", *filename)

	/* Read file */
	raw, err := ioutil.ReadAll(data)
	if err != nil {
		panic("Failed to read the data from '" + *filename + "':" + err.Error())
	}

	/* Parse file */
	if err := json.Unmarshal(raw, &config); err != nil {
		panic("Failed to parse configuration file '" + *filename + "':" + err.Error())
	}

	/* Launch all Go Routines */
	for _, c := range config {
		if c.Retry == 0 {
			c.Retry = 5
		}
		if c.Timeout == 0 {
			c.Timeout = 10
		}
		go knock(c)
	}

	/* Sleep indefinitely */
	for {
		time.Sleep(1000 * time.Second)
	}
}

func log(l LogEntry) {
	if j, err := json.Marshal(l); err == nil {
		fmt.Printf("%s\n", j)

	}
}

func knock(o Osp) {

	/* Fetches a URL based on the contents of Osp struct */

	/* Loop forever */
	for {

		/* First sleep */
		time.Sleep(time.Duration(o.Interval) * time.Second)

		/* Then launch the fetcher in the background */
		/* It is launched in the background (as a goroutine) to avoid skewing the time interval */

		go func(o Osp) {

			var l LogEntry

			/* Create custom client to set timeout */
			client := &http.Client{
				Timeout: time.Second * time.Duration(o.Timeout),
			}

			for retry := 0; retry < o.Retry; retry++ {

				start := time.Now()

				request, err := http.NewRequest(o.Method, o.URL, nil)
				if err != nil {
					fmt.Println(err.Error())
					continue
				}

				response, err := client.Do(request)

				l.Start = start.Format("2006-01-02T15:04:05.999999-07:00")
				l.Duration = time.Now().Sub(start).Seconds()
				l.Retries = retry + 1
				l.MaxRetries = o.Retry
				l.Target = o.URL

				if err == nil && response.Status == "200 OK" {
					l.Level = "info"
					l.Status = response.Status
					log(l)
					return
				}

			}

			l.Level = "error"
			log(l)

		}(o)
	}
}
